import numpy as np
import torch
from torch import tensor
from torch.autograd import Variable

from utils.plot_utils import plot_function, plot_fgrads


class CDiscriminantFunction(object):

    def __init__(self, W, b):
        """
        This function initialize the discriminant function object.

        :param W: Weight of the linear combination.
        :param b: Bias of the linear combination.
        """
        self._W = tensor(torch.from_numpy(W))
        self._b = tensor(torch.from_numpy(b))

    def fun(self, X):
        """
        This function compute the result of the linear combination of the
        sample features.

        f(x) = X W + b

        :param x: numpy array with shape (n * d) where n is the number of
        samples and d is the number of features taken into account by the
        system.

        :return:
        """
        self._x = Variable(torch.from_numpy(X), requires_grad=True)
        self._fun_T = torch.matmul(self._x, self._W) + self._b
        return self._fun_T.data.numpy()

    def fgrad(self, x):
        """
        This function compute the gradient the linear combination w.r.t. a
        single sample

        :param x: numpy array with dimension (1 * d)
        :return:
        """
        x = np.atleast_2d(x)

        if x.shape[0] > 1:
            raise ValueError("You cannot compute the gradient w.r.t. "
                             "multiple samples ")

        self.fun(x)
        self._fun_T.backward()
        grad = np.array(self._x.grad.data.numpy())
        self._x.grad.data.zero_()
        return grad


################# let's try to use the functions that we have just defined:

n = 2
d = 2

# generate randomly the parameters of two samples
X = np.random.rand(n, d)

# generate randomly the parameters of the function
W = np.random.rand(d, 1) * 100
b = np.random.rand(1)

discr_fun = CDiscriminantFunction(W, b)

print discr_fun.fun(X)

n = 1
X = np.random.rand(n, d)

print "gradient : ", discr_fun.fgrad(X)

print "W : ", W

plot_function(discr_fun.fun)
plot_fgrads(discr_fun.fgrad)
