import numpy as np
import matplotlib.pyplot as plt

def _create_grid_points(x_min=-1, x_max=1, y_min=-1, y_max=1):
    h = .05  # step size of the grid
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))

    # Compute the values of the function. For that, we will assign a color
    # point in the mesh [x_min, x_max]x[y_min, y_max].
    mesh_points = np.c_[xx.ravel(), yy.ravel()]
    return xx, yy, mesh_points


def _iter_fun(fun, X):
    n_points = X.shape[0]
    res = np.zeros(shape=n_points, )
    for i in xrange(n_points):
        res[i] = fun(X[i, :])
    return res


def _iter_gfun(fun, X):
    n_points = X.shape[0]
    n_feats = X.shape[1]
    res = np.zeros(shape=(n_points, n_feats))
    for i in xrange(n_points):
        res[i, :] = fun(X[i, :])
    return res


def plot_function(fun, x_min=-1, x_max=1, y_min=-1, y_max=1):
    xx, yy, mesh_points = _create_grid_points(x_min=x_min, x_max=x_max,
                                              y_min=y_min,
                                              y_max=y_max)

    Z = _iter_fun(fun, mesh_points)

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    cm = plt.cm.bwr
    ax = plt.contourf(xx, yy, Z, cmap=cm, alpha=.8)
    plt.colorbar(ax, orientation='vertical')


def plot_fgrads(gfun, x_min=-1, x_max=1, y_min=-1, y_max=1):
    xx, yy, mesh_points = _create_grid_points(x_min=x_min, x_max=x_max,
                                              y_min=y_min,
                                              y_max=y_max)

    grad_point_values = _iter_gfun(gfun, mesh_points)

    plt.xlim((x_min, x_max))
    plt.ylim((y_min, y_max))

    U = grad_point_values[:, 0].reshape(
        (xx.shape[0], xx.shape[1]))
    V = grad_point_values[:, 1].reshape(
        (yy.shape[0], yy.shape[1]))

    plt.quiver(xx, yy, U, V,  units='width')

    plt.tight_layout()
    plt.show()

def gfun(x):
    x = np.atleast_2d(x)
    y = np.ones(shape=x.shape[1])
    return y

