import torch

# create a tensor (it is pretty similar to a numpy array)
t = torch.tensor([[1., 3]])

print t

# you can get some element from it
print t[0, 1]

# you can set some element with a different value
t[0, 0] = 5

print t

#################################

from torch.autograd import Variable

# create a variable specifying that we would like to compute the gradient of
# the operations that we made with this variable.
# This make pytorch create the graph that will allows us to creat the gradient
x = Variable(t, requires_grad=True)

#create a random tensor
t = torch.randn(2, 2)

print t

#set the element of a variable equal to a different value
x.data[0, 0] = 8

print x

# make some operation with the variable x
a = (x * x).sum()

print a

# compute the gradient of a w.r.t. all the variable which are involved in the
# computation of a and store the value of the gradient inside the varibles
# then delete the graph
a.backward()

print x.grad

#############################

# let's try to do the same operations again
a = (x * x).sum()
a.backward()

# do you expect to see the same gradient that you have seen before?
# the gradient is computed and summed to the gradient that we have computed
# before
print "g1 : ",  x.grad

# We can zero the gradient before to do the operations again
x.grad.data.zero_()
print "g2 : ", x.grad

a = (x * x).sum()
a.backward()

# We will get the same gradient that we got before
print "g3 : ", x.grad

####################### some other useful functionalities :

import numpy as np

# create a variable from a numpy array
b = np.array([3., 5])
c = Variable(torch.from_numpy(b), requires_grad=True)

# we can cast a tesnsor to a numpy array
print t.numpy()

# we can cast a variable to a numpy array
print c.data.numpy()
