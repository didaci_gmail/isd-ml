from abc import ABCMeta, abstractmethod

class CClassifier(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def fit(self):
        raise NotImplementedError

    @abstractmethod
    def predict(self):
        raise NotImplementedError